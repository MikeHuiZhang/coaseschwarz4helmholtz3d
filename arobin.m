function re= arobin(coord)
%This function reads(when called for the first time) the piecewise constant
% velocity from the input 'datafile' and gives the wavenumber= omega/velocity
% at the point specified by the input 'coord'
global data_array;                % change this file along with ksquare.m
nz= 2; ny= 6; nx= 6;   freq= 0.25;  % change here
hz= 4200/nz; hy= 13520/ny; hx= 13520/nx; 
iz= floor(coord(:,3)/hz)+1;
iy= floor(coord(:,2)/hy)+1;
ix= floor(coord(:,1)/hx)+1;
iz(iz>nz)= nz; 
iy(iy>ny)= ny; 
ix(ix>nx)= nx; 
ii= (ix-1)*ny*nz + (iy-1)*nz + iz; 
re= -1i*2*pi*freq./data_array(ii);  % change here for freq 
end    