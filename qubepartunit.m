function Dw= qubepartunit(nx,ny,nz,Nx,Ny,Nz,wl,sdiri,withdiri)
% sdiri should be for 'wl' overlapping subdomains

snx= nx/Nx;  sny= ny/Ny;  snz= nz/Nz; Ns= Nx*Ny*Nz; 
Dw= cell(Ns,1);
for s= 1:Ns
   [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
   [sy,sz]= ind2sub([Ny,Nz],sy); 
   % - vectors supporting on subdomains
   % -- the nodes on the non-overlapping sub. have value 1
   ix= ((sx-1)*snx+1):(sx*snx+1); % 3d position 
   iy= ((sy-1)*sny+1):(sy*sny+1); % of global nodes
   iz= ((sz-1)*snz+1):(sz*snz+1);
   [IX,IY,IZ]= ndgrid(ix,iy,iz);  
   clear ix iy iz;
   ii= sub2ind([nx+1,ny+1,nz+1],IX(:),IY(:),IZ(:));
   vi= ones(length(ii),1);
   % -- the nodes in the outside boundary layers
   %    we first give (3d position of bottom left corner)-1 for each layer
   %    and we generate the local 3d position for each layer
   %    then we add the two results to give the global 3d position
   numwl= 0; layerfaces= [sx>1,sx<Nx,sy>1,sy<Ny,sz>1,sz<Nz];
   for layer= 1:wl-1
       numl= qubelayer(snx+(sx>1)*layer+(sx<Nx)*layer, ...
           sny+(sy>1)*layer+(sy<Ny)*layer,snz+(sz>1)*layer+(sz<Nz)*layer,layerfaces);
       numwl= numwl + numl;
   end
   ib= zeros(numwl,1); vb= zeros(numwl,1); numwl= 0;
   dispx= (sx-1)*snx; dispy= (sy-1)*sny; dispz= (sz-1)*snz;
   for layer= 1:wl-1
       dispx= dispx-(sx>1);  dispy= dispy-(sy>1);  dispz= dispz-(sz>1);
       [numl,IX,IY,IZ]= qubelayer(snx+(sx>1)*layer+(sx<Nx)*layer, ...
           sny+(sy>1)*layer+(sy<Ny)*layer,snz+(sz>1)*layer+(sz<Nz)*layer,layerfaces);
       ib(numwl+1:numwl+numl)= sub2ind([nx+1,ny+1,nz+1],IX(:)+dispx,IY(:)+dispy,IZ(:)+dispz);
       vb(numwl+1:numwl+numl)= 1-layer/wl;
       numwl= numwl+numl;
   end
   Dw{s}= sparse([ii;ib],1,[vi;vb],(nx+1)*(ny+1)*(nz+1),1,length(ii)+length(ib));
   clear ii ib vi vb;
end
% finish Dw
Dtotal= sparse((nx+1)*(ny+1)*(nz+1),1);
for s= 1:Ns
      Dtotal= Dtotal+Dw{s};
end
for s= 1:Ns
     Dw{s}= Dw{s}./Dtotal;
     if ~exist('withdiri','var') || withdiri==0
         Dw{s}(sdiri{s})= 0;
     end
end 

end  % -- end of the function qubepartunit