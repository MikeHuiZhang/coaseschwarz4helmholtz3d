function Se= stima2d(vertices,a1,a2)
% INPUT vertices: 3d coordinates of a rectangle element
%              a: we will do (au,v) on the element
% OUTPUT      Se: element matrix associated with (au,v)
global M2d;
% global gp2d gw2d gp2d1 gw2d1;
% persistent phi;
% Jacobian of the transformation from [-1,1]^2 to the element in 3d
% coordinates, 3 by 2 matrix
DPhi= [vertices(2,:)-vertices(1,:);vertices(4,:)-vertices(1,:)]'/2;
detDPhi= norm(DPhi(:,1))*norm(DPhi(:,2)); % ratio of areas 
if nargin==2
    if isa(a1,'float') 
        Se= a1*detDPhi*M2d;
    elseif isa(a1,'inline') || isa(a1,'function_handle')
        Se= a1(mean(vertices,1))*detDPhi*M2d;
    end
elseif nargin==3
    if isa(a1,'float') 
       Se= a1*detDPhi*M2d;
       B = inv ( DPhi' * DPhi ); 
       C1 =  [  2, -2; -2,  2 ] * B(1,1) ...
           + [  3,  0;  0, -3 ] * B(1,2) ...
           + [  2,  1;  1,  2 ] * B(2,2);
       C2 =  [ -1,  1;  1, -1 ] * B(1,1) ...
           + [ -3,  0;  0,  3 ] * B(1,2) ...
           + [ -1, -2; -2, -1 ] * B(2,2);
       Se = Se + a2*detDPhi * [ C1 C2; C2 C1 ] / 6; % no modification from advection/stima4.m 'cause detDPhi and B 
    elseif isa(a1,'inline') || isa(a1,'function_handle')
       mvertice= mean(vertices,1);
       Se= feval(a1,mvertice)*detDPhi*M2d;
       B = inv ( DPhi' * DPhi ); 
       C1 =  [  2, -2; -2,  2 ] * B(1,1) ...
           + [  3,  0;  0, -3 ] * B(1,2) ...
           + [  2,  1;  1,  2 ] * B(2,2);
       C2 =  [ -1,  1;  1, -1 ] * B(1,1) ...
           + [ -3,  0;  0,  3 ] * B(1,2) ...
           + [ -1, -2; -2, -1 ] * B(2,2);
       Se = Se + feval(a2,mvertice)*detDPhi * [ C1 C2; C2 C1 ] / 6; 
    end
end

 
%     % values of the four basis functions at gauss quadrature points
%     if isempty(phi)
%         if ~isempty(gp2d1)
%             phi= fembasis2d(gp2d1);
%         else
%             phi= fembasis2d(gp2d);
%         end
%     end
%     % gauss quadrature points on the element of 'vertices'
%     if ~isempty(gp2d1)
%         DPhigp= (1+gp2d1)*DPhi'+repmat(vertices(1,:),size(gp2d1,1),1);
%         Se= detDPhi*phi'*(repmat(gw2d1.*a(DPhigp),1,4).*phi); 
%     else
%         DPhigp= (1+gp2d)*DPhi'+repmat(vertices(1,:),size(gp2d,1),1);
%         % faster than repmat
%         va= gw2d.*a(DPhigp);
%         Se= detDPhi*phi'*([va va va va].*phi);
% %         Se= detDPhi*phi'*(repmat(gw2d.*a(DPhigp),1,4).*phi);
%     end
end