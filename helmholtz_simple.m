function helmholtz_simple()

clc;
clear all;
% The Helmholtz problem is
%    -\lap u - k^2 u = f,   in \Omega
%                 u = uD,   on \Gamma_D
%       du/dn + a u = uR,   on \Gamma_R
% where a can be zero for Neumann boundary.



% example 3
Lz= 4200; Ly= 13520; Lx= 13520;
f= 0;  pointsource= [6000,6760,10,1];
uD= 0;
uR= 0;

% geometry: continuous description of the qube domain and boundary
if exist('Lx','var')
    xl= 0;  xr= Lx;
    yl= 0;  yr= Ly;
    zl= 0;  zr= Lz;
else
    xl= 0; xr= 1;
    yl= 0; yr= 1;
    zl= 0; zr= 1;
end
GammaD= []; % 1 left, 2 right, 3 front, 4 back, 5 bottom, 6 top 
GammaR= [1,2,3,4,5,6];

% discretization parameter
nx= 24; ny= nx; nz= nx/3;
% hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;

% fem direct solution on the whole mesh 
% [coordinates,elements,dirichlet,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
% disp('fem');
% tic
% udirect= femdirect(@ksquare,f,pointsource,uD,@arobin,uR,coordinates,elements,dirichlet,robin);
% toc

% ddm solution
Nxv= 3;  Nyv= 3;  Nzv= 1;
% diary(num2str(clock));
for numrun= 1:length(Nxv)
    
% partition to Nx X Ny X Nz subdomains, fetidp require Nx > freq*Lx/c/sqrt(2)/pi    
Nx= Nxv(numrun); Ny= Nyv(numrun); Nz= Nzv(numrun); 
tdd= tic;
[udd,flag,relres,iter,resvec]= ...
    schwarzsubs(@ksquare,f,pointsource,uD,@arobin,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,...
      GammaR,Nx,Ny,Nz,1,[],[],6);
postprocess();
toc(tdd);

end
%diary off;

    function postprocess()
        if exist('resvec','var') && ~isempty(resvec)
            figure;
            semilogy(resvec);
            xlabel('iteration');
            ylabel('residual');
            disp('relres='); disp(relres);
        end
        if exist('udd','var') && ~isempty(udd)
            disp('iter='); disp(iter);
            switch flag
                case 1
                    disp('gmres reached maximum iteration number but did not converge');
                case 2
                    disp('preconditioner M is ill-conditioned');
                case 3
                    disp('gmres stagnated (two consective iterates were the same)');
            end
        end
        if (exist('udirect','var') && exist('udd','var') && ~isempty(udd))
            errdd2fem= abs(udd - udirect);
            maxerrdd2fem= max(errdd2fem);
            disp('maxerrdd2fem='); disp(maxerrdd2fem);
            %     [X,Y,Z]= meshgrid(xl:hx:xr,yl:hy:yr,zl:hz:zr);
            %     errdd2fem= reshape(errdd2fem,nx+1,ny+1,nz+1);
            %     slice(X,Y,Z,errdd2fem, 0.5, 0.5, 0.5);
            %     xlabel('x'); ylabel('y'); zlabel('z');
            %     colorbar;
            %     figure;
            %     [X2,Y2]= ndgrid(xl:hx:xr,yl:hy:yr);
            %     surf(X2,Y2,errdd2fem(:,:,nz/2+1)); xlabel('x'); ylabel('y');
        end
        if exist('uexact','var') && exist('udirect','var') && exist('coordinates','var')
            errfem2cont= uexact(coordinates) - udirect;
            maxerrfem2cont= max(abs(errfem2cont)); % compare to uexact
            disp('maxerrfem2cont='); disp(maxerrfem2cont);
        end
        if (exist('specF','var')) && ~isempty(specF)
            figure;
            plot(specF,'o','MarkerSize',6);
        end       
    end  % -- end of function postprocess
end