function [u,flag,relres,iter,resvec,specF]= ...
    schwarzsubs(k2,f,pointsource,uD,a,uR,xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR, ...
           Nx,Ny,Nz,ol,wl,cform,ndtn)
%obddsubs: the substructured form of obdd.m
% no partition of unit, the communication relation is decomposed to
% faces, edges and vertices

%% partition of the global mesh into overlapping submeshes
tsetup= tic;
fprintf('\npartition\n');
tpart= tic;
[coordinates,elements,~,robin]= qubemesh(xl,xr,yl,yr,zl,zr,nx,ny,nz,GammaD,GammaR);
[Ro,sdiri,sbdnodes,sbdelems,srobinelems,selems]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,ol,1);
[sfnodes,sendf,sfsend,ngf,senodes,sende,sesend,nge,...
    svnodes,sendv,svsend,ngv]= ...
    qubecommedge(GammaD,nx,ny,nz,Nx,Ny,Nz,ol);
disp('size of interface variables'); disp(ngf+nge+ngv);
Ns= Nx*Ny*Nz; nn= (nx+1)*(ny+1)*(nz+1);
NumCommEdges= 0;  ngb= ngf + nge + ngv;
% partition of unity for recovering u
if ~exist('wl','var')
    wl= ol+1;
end
[~,sdiriw]= qubesubmesh(GammaD,GammaR,nx,ny,nz,Nx,Ny,Nz,wl);
Dw= qubepartunit(nx,ny,nz,Nx,Ny,Nz,wl,sdiriw,1);
clear sdiriw;
toc(tpart);

%% calculate elements stiffness and loads
fprintf('\nelements stiffness and loads\n');
tag= tic;
getAM(); getgpw(); % for use by stima.m and rhs.m
getM2d(); getgpw2d(); % for use by stima2d and rhs2d
global unused_src numsrc; % for use by rhs.m
[I,J]= ndgrid(1:8,1:8); I= I(:); J= J(:);
[Ir,Jr]= ndgrid(1:4,1:4); Ir= Ir(:); Jr= Jr(:);
ne= size(elements,1);
ii= zeros(64,ne); jj= zeros(64,ne); Aa= zeros(64,ne);
for j= 1:ne
    Ae= stima(coordinates(elements(j,:),:),k2);
    ii(:,j)= elements(j,I); % assemble vectors
    jj(:,j)= elements(j,J); % for speed
    Aa(:,j)= Ae(:);
end
iib= []; ba= [];
if ~(isa(f,'float') && 0==f && isempty(pointsource))
   iib= zeros(8,ne); ba= zeros(8,ne);
   for j = 1:ne
      n4e= elements(j,:);
      iib(:,j)= n4e;
      ba(:,j)= rhs(coordinates(n4e,:),f,pointsource);
   end
end
% treat outer Robin b.c.
nr= size(robin,1); 
ir= []; jr= []; Ar= [];
if ~(isa(a,'float') && 0==a)
   ir= zeros(16,nr); jr= zeros(16,nr); Ar= zeros(16,nr);
   for j = 1:nr
       ir(:,j)= robin(j,Ir);
       jr(:,j)= robin(j,Jr); 
       Ae= stima2d(coordinates(robin(j,:),:),a);
       Ar(:,j)= Ae(:);
   end
end
irb= []; br= [];
if ~(isa(uR,'float') && 0==uR)
   irb= zeros(4,nr); br= zeros(4,nr);
   for j = 1:nr
      n4e= robin(j,:);
      irb(:,j)= n4e;
      br(:,j)= rhs2d(coordinates(n4e,:),uR);
   end
end
clear robin;
unused_src= []; numsrc= [];
toc(tag);

%% assemble subdomains matrices and rhs
fprintf('\nassembly subdomains\n');
tas= tic;
A= cell(Ns,1);  b= cell(Ns,1); rhsl= zeros(ngf+nge+ngv,1); 
Aget= cell(Ns,1); Agive= cell(Ns,1);
Rb= cell(Ns,1); AN= cell(Ns,1); Mb= cell(Ns,1); Lb= cell(Ns,1);
for s= 1:Ns
    Agive{s}= sparse(ngf+nge+ngv,nn);
end
for s= 1:Ns
    % solid elements
    iis= ii(:,selems{s}); iis= iis(:);
    jjs= jj(:,selems{s}); jjs= jjs(:);
    Aas= Aa(:,selems{s}); Aas= Aas(:);
    iibs= []; bas= [];
    if ~(isa(f,'float') && 0==f && isempty(pointsource))
        iibs= iib(:,selems{s});
        bas= ba(:,selems{s});
    end
    selems{s}= [];
    % outer Robin
    if ~isempty(ir)
        irs= ir(:,srobinelems{s}); irs= irs(:);
        jrs= jr(:,srobinelems{s}); jrs= jrs(:);
        Ars= Ar(:,srobinelems{s}); Ars= Ars(:);
    else
        irs= [];
        jrs= [];
        Ars= [];
    end
    irbs= []; brs= [];
    if ~(isa(uR,'float') && 0==uR)
       irbs= irb(:,srobinelems{s}); 
       brs= br(:,srobinelems{s});
    end
    srobinelems{s}= [];
    % setup a global matrix and a rhs for the subdomain
    Ag= sparse([iis;irs],[jjs;jrs],[Aas;Ars],nn,nn);
    clear iis jjs Aas irs jrs Ars;
    bg= sparse([iibs(:);irbs(:)],1,[bas(:);brs(:)],nn,1);
    clear iibs bas irbs brs;
    % treatment of the outer Dirichlet
     if ~isempty(sdiri{s})
        if isa(uD,'float')
            bg= setsparse(bg,sdiri{s},ones(length(sdiri{s}),1),uD);
        elseif isa(uD,'inline') || isa(uD,'function_handle') 
            bg= setsparse(bg,sdiri{s},ones(length(sdiri{s}),1),uD(coordinates(sdiri{s},:)));
        end
    end
    [iid, jjd]= find(Ag);
    if ~isempty(sdiri{s})
        idxofzero = ismember(iid,sdiri{s});
        Ag= setsparse(Ag,iid(idxofzero),jjd(idxofzero),0);
        Ag= setsparse(Ag,sdiri{s},sdiri{s},1);
        clear iid jjd idxofzero;
    end
    % restrict the Neumann matrix to the subdomain
    AN{s}= Ro{s}*Ag*Ro{s}.';
    % treatment of internal Dirichlet
    bg= setsparse(bg,sbdnodes{s},ones(length(sbdnodes{s}),1),0);
    [iid, jjd]= find(Ag);
    idxofzero= ismember(iid,sbdnodes{s});
    Ag= setsparse(Ag,iid(idxofzero),jjd(idxofzero),0);
    Ag= setsparse(Ag,sbdnodes{s},sbdnodes{s},1);
    clear iid jjd idxofzero;
    % restrict to the subdomain
    A{s}= Ro{s}*Ag*Ro{s}.';
    b{s}= Ro{s}*bg;
    clear Ag; 
    clear bg;
    % restrict to communication edges
    Aget{s}= sparse(size(Ro{s},1),ngf+nge+ngv);
    [sx,sy]= ind2sub([Nx,Ny*Nz],s); % 3d position of this subdomain
    [sy,sz]= ind2sub([Ny,Nz],sy);
    % - faces
    if 1==s
        sdisp= 0;
    else
        sdisp= sendf(s-1);
    end
    for m= 1:6
        if 1==m
            idx= 1:sfsend(1,s);
        else
            idx= sfsend(m-1,s)+1:sfsend(m,s);
        end
        Rf= sparse(1:length(idx),sfnodes{s}(idx),1,length(idx),nn);
        Ef= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Rf)
            NumCommEdges= NumCommEdges + 1;
            Aget{s}= Aget{s} + Ro{s}*(Rf.'*( Ef.' ));
        end
        if 1==m
           % - - left face
           if sx > 1
               s2= s-1;
           end
        elseif 2==m
           % - -right face
           if sx < Nx
               s2= s+1;
           end
        elseif 3==m
           % - - front face
           if sy > 1
               s2= s-Nx;
           end
        elseif 4==m
           % - - back face
           if sy < Ny
               s2= s+Nx;
           end
        elseif 5==m
            % - - bottom face
            if sz > 1
                s2= s-Nx*Ny;
            end
        elseif 6==m
            % - - top face
            if sz < Nz
                s2= s+Nx*Ny;
            end
        end
        if ~isempty(Ef)
            Agive{s2}= Agive{s2} + Ef*Rf;
        end
        clear Rf Ef;
    end
    % edges
    if 1==s
        sdisp= ngf;
    else
        sdisp= ngf + sende(s-1);
    end
    for m= 1:12
        if 1==m
            idx= 1:sesend(1,s);
        else
            idx= sesend(m-1,s)+1:sesend(m,s);
        end
        Re= sparse(1:length(idx),senodes{s}(idx),1,length(idx),nn);
        Ee= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Re)
            Aget{s}= Aget{s} + Ro{s}*(Re.'*( Ee.' ));
            NumCommEdges= NumCommEdges + 1;
        end
        if 1==m
           % - - - - 1
           if sx>1 && sy>1
               s2= s-Nx-1;
           elseif 1==sx && sy>1
               s2= s-Nx;
           elseif sx>1 && sy==1
               s2= s-1;
           end
        elseif 2==m
           % - - - - 2
           if sx<Nx && sy>1
               s2= s-Nx+1;
           elseif sx==Nx && sy>1
               s2= s-Nx;
           elseif sx<Nx && sy==1
               s2= s+1;
           end
        elseif 3==m
           % - - - - 3
           if sx>1 && sy<Ny
               s2= s+Nx-1;
           elseif sx==1 && sy<Ny
               s2= s+Nx;
           elseif sx>1 && sy==Ny
               s2= s-1;
           end
        elseif 4==m
           % - - - - 4
           if sx<Nx && sy<Ny
               s2= s+Nx+1;
           elseif sx==Nx && sy<Ny  
               s2= s+Nx;
           elseif sx<Nx && sy==Ny
               s2= s+1;
           end
        elseif 5==m
           % - - - - 5
           if sx>1 && sz>1
               s2= s-Nx*Ny-1;
           elseif sx==1 && sz>1
               s2= s-Nx*Ny;
           elseif sx>1 && sz==1
               s2= s-1;
           end
        elseif 6==m
            % - - - - 6
            if sx<Nx && sz>1
               s2= s-Nx*Ny+1;
            elseif sx==Nx && sz>1
               s2= s-Nx*Ny;
            elseif sx<Nx && sz==1
               s2= s+1;
            end
        elseif 7==m
           % - - - - 7
           if sx>1 && sz<Nz
               s2= s+Nx*Ny-1;
           elseif sx==1 && sz<Nz
               s2= s+Nx*Ny;
           elseif sx>1 && sz==Nz
               s2= s-1;
           end
        elseif 8==m
           % - - - - 8
           if sx<Nx && sz<Nz
               s2= s+Nx*Ny+1;
           elseif sx==Nx && sz<Nz
               s2= s+Nx*Ny;
           elseif sx<Nx && sz==Nz
               s2= s+1;
           end
        elseif 9==m
           % - - - - 9
           if sy>1 && sz>1
               s2= s - Nx - Nx*Ny;
           elseif sy==1 && sz>1
               s2= s - Nx*Ny;
           elseif sy>1 && sz==1
               s2= s - Nx;
           end
        elseif 10==m
           % - - - - 10
           if sy<Ny && sz>1
               s2= s + Nx - Nx*Ny;
           elseif sy==Ny && sz>1
               s2= s - Nx*Ny;
           elseif sy<Ny && sz==1
               s2= s + Nx;
           end
        elseif 11==m
           % - - - - 11
           if sy>1 && sz<Nz
               s2= s - Nx + Nx*Ny;
           elseif sy==1 && sz<Nz
               s2= s + Nx*Ny;
           elseif sy>1 && sz==Nz
               s2= s - Nx;
           end
        elseif 12==m
           % - - - - 12
           if sy<Ny && sz<Nz
              s2= s + Nx + Nx*Ny;
           elseif sy==Ny && sz<Nz
              s2= s + Nx*Ny;
           elseif sy<Ny && sz==Nz
              s2= s + Nx;
           end
        end
        if ~isempty(Ee)
            Agive{s2}= Agive{s2} + Ee*Re;
        end
        clear Re Ee;
    end
     % vertices
    if 1==s
        sdisp= ngf + nge;
    else
        sdisp= ngf + nge + sendv(s-1);
    end
    for m= 1:8
        if 1==m
            idx= 1:svsend(1,s);
        else
            idx= svsend(m-1,s)+1:svsend(m,s);
        end
        Rv= sparse(1:length(idx),svnodes{s}(idx),1,length(idx),nn);
        Ev= sparse(idx+sdisp,1:length(idx),1,ngb,length(idx));
        if ~isempty(Rv)
            Aget{s}= Aget{s} + Ro{s}*(Rv.'*( Ev.' ));
            NumCommEdges= NumCommEdges + 1;
        end
        if 1==m
            % - - - - 1
            if sx>1 && sy>1 && sz>1
                s2= s-1-Nx-Nx*Ny;
            elseif sx==1 && sy>1 && sz>1
                s2= s-Nx-Nx*Ny;
            elseif sx==1 && sy==1 && sz>1
                s2= s-Nx*Ny;
            elseif sx==1 && sy>1 && sz==1
                s2= s-Nx;
            elseif sx>1 && sy==1 && sz>1
                s2= s-1-Nx*Ny;
            elseif sx>1 && sy==1 && sz==1
                s2= s-1;
            elseif sx>1 && sy>1 && sz==1
                s2= s-1-Nx;
            end
        elseif 2==m
            % - - - - 2
            if sx<Nx && sy>1 && sz>1
                s2= s + 1 - Nx - Nx*Ny;
            elseif sx==Nx && sy>1 && sz>1
                s2= s - Nx - Nx*Ny;
            elseif sx==Nx && sy==1 && sz>1
                s2= s - Nx*Ny;
            elseif sx==Nx && sy>1 && sz==1
                s2= s - Nx;
            elseif sx<Nx && sy==1 && sz>1
                s2= s + 1 - Nx*Ny;
            elseif sx<Nx && sy==1 && sz==1
                s2= s + 1;
            elseif sx<Nx && sy>1 && sz==1
                s2= s + 1 - Nx;
            end
        elseif 3==m
            % - - - - 3
            if sx<Nx && sy<Ny && sz>1
                s2= s + 1 + Nx - Nx*Ny;
            elseif sx==Nx && sy<Ny && sz>1
                s2= s + Nx - Nx*Ny;
            elseif sx==Nx && sy==Ny && sz>1
                s2= s - Nx*Ny;
            elseif sx==Nx && sy<Ny && sz==1
                s2= s + Nx;
            elseif sx<Nx && sy==Ny && sz>1
                s2= s + 1 - Nx*Ny;
            elseif sx<Nx && sy==Ny && sz==1
                s2= s + 1;
            elseif sx<Nx && sy<Ny && sz==1
                s2= s + 1 + Nx;
            end
        elseif 4==m
            % - - - - 4
            if sx>1 && sy<Ny && sz>1
                s2= s -1 + Nx - Nx*Ny;
            elseif sx==1 && sy<Ny && sz>1
                s2= s + Nx - Nx*Ny;
            elseif sx==1 && sy==Ny && sz>1
                s2= s - Nx*Ny;
            elseif sx==1 && sy<Ny && sz==1
                s2= s + Nx;
            elseif sx>1 && sy==Ny && sz>1
                s2= s -1 - Nx*Ny;
            elseif sx>1 && sy==Ny && sz==1
                s2= s - 1;
            elseif sx>1 && sy<Ny && sz==1
                s2= s - 1 + Nx ;
            end
        elseif 5==m
            % - - - - 5
            if sx>1 && sy>1 && sz<Nz
                s2= s - 1 - Nx + Nx*Ny;
            elseif sx==1 && sy>1 && sz<Nz
                s2= s - Nx + Nx*Ny;
            elseif sx==1 && sy==1 && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==1 && sy>1 && sz==Nz
                s2= s - Nx;
            elseif sx>1 && sy==1 && sz<Nz
                s2= s - 1 + Nx*Ny;
            elseif sx>1 && sy==1 && sz==Nz
                s2= s - 1;
            elseif sx>1 && sy>1 && sz==Nz
                s2= s - 1 - Nx;
            end
        elseif 6==m
            if sx<Nx && sy>1 && sz<Nz
               s2= s + 1 - Nx + Nx*Ny;
            elseif sx==Nx && sy>1 && sz<Nz
                s2= s - Nx + Nx*Ny;
            elseif sx==Nx && sy==1 && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==Nx && sy>1 && sz==Nz
                s2= s - Nx;
            elseif sx<Nx && sy==1 && sz<Nz
                s2= s + 1 + Nx*Ny;
            elseif sx<Nx && sy==1 && sz==Nz
                s2= s + 1;
            elseif sx<Nx && sy>1 && sz==Nz
                s2= s + 1 - Nx;
            end
        elseif 7==m
            % - - - - 7
            if sx<Nx && sy<Ny && sz<Nz
                s2= s + 1 + Nx + Nx*Ny;
            elseif sx==Nx && sy<Ny && sz<Nz
                s2= s + Nx + Nx*Ny;
            elseif sx==Nx && sy==Ny && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==Nx && sy<Ny && sz==Nz
                s2= s + Nx;
            elseif sx<Nx && sy==Ny && sz<Nz
                s2= s + 1 + Nx*Ny;
            elseif sx<Nx && sy==Ny && sz==Nz
                s2= s + 1;
            elseif sx<Nx && sy<Ny && sz==Nz
                s2= s + 1 + Nx;
            end
        elseif 8==m
            % - - - - 8
            if sx>1 && sy<Ny && sz<Nz
                s2= s - 1 + Nx + Nx*Ny;
            elseif sx==1 && sy<Ny && sz<Nz
                s2= s + Nx + Nx*Ny;
            elseif sx==1 && sy==Ny && sz<Nz
                s2= s + Nx*Ny;
            elseif sx==1 && sy<Ny && sz==Nz
                s2= s + Nx;
            elseif sx>1 && sy==Ny && sz<Nz
                s2= s - 1 + Nx*Ny;
            elseif sx>1 && sy==Ny && sz==Nz
                s2= s - 1;
            elseif sx>1 && sy<Ny && sz==Nz
                s2= s - 1 + Nx;
            end
        end
        if ~isempty(Ev)
            Agive{s2}= Agive{s2} + Ev*Rv;
        end
        clear Rv Ev;
    end
    clear idx sdisp;
    % prepare matrices for localDtN operator
    if ndtn~=0
        % - matrix from global interface to the local interface
        nsb= length(sfnodes{s})+length(senodes{s})+length(svnodes{s});
        if 1==s
            Rb{s}= sparse(1:nsb,[1:sendf(1),ngf+1:ngf+sende(1), ...
                ngf+nge+1:ngf+nge+sendv(1)], 1, nsb, ngf+nge+ngv);
        else
            Rb{s}= sparse(1:nsb,[sendf(s-1)+1:sendf(s),ngf+sende(s-1)+1:ngf+sende(s), ...
                ngf+nge+sendv(s-1)+1:ngf+nge+sendv(s)], 1, nsb, ngf+nge+ngv);
        end
        % - matrix from the subdomain to its local interface
        %   for the classical Schwarz Aget{s} is the restriction from
        %   the global interface to the subdomain
        Lb{s}= Rb{s}*Aget{s}';
        % - interface mass matrix
        nb= size(sbdelems{s},1);
        ib= zeros(16,nb); jb= zeros(16,nb); Ab= zeros(16,nb);
        for j = 1:nb
            ib(:,j)= sbdelems{s}(j,Ir);
            jb(:,j)= sbdelems{s}(j,Jr);
            Ae= stima2d(coordinates(sbdelems{s}(j,:),:),1);
            Ab(:,j)= Ae(:);
        end
        Ag= sparse(ib(:),jb(:),Ab(:),nn,nn);
        Mb{s}= Lb{s}*(Ro{s}*Ag*Ro{s}')*Lb{s}';
    end
    clear ib jb Ab Ae Ag;
end
clear elements selems sdiri srobinelems;
clear ii jj Aa ir jr Ar iib ba irb br Ir Jr I J;
toc(tas);

%% LU of subdomain matrices
fprintf('\nsubdomains lu\n');
tsub= tic;
L= cell(Ns,1); U= cell(Ns,1); P= cell(Ns,1); Q= cell(Ns,1);
for s= 1:Ns
   [L{s},U{s},P{s},Q{s}]= lu(A{s});
   A{s}= [];
end
clear A;
% LU of Neumann subproblems for the localNtD
if ndtn~=0
    LN= cell(Ns,1); UN= cell(Ns,1); PN= cell(Ns,1); QN= cell(Ns,1);
    for s= 1:Ns
        [LN{s},UN{s},PN{s},QN{s}]= lu(AN{s});
    end
end
% LU of Mb
LMb= cell(Ns,1); UMb= cell(Ns,1); PMb= cell(Ns,1); QMb= cell(Ns,1);
for s= 1:Ns
   [LMb{s},UMb{s},PMb{s},QMb{s}]= lu(Mb{s});
end
toc(tsub);
disp('setup mostly finished');
toc(tsetup);

%% prepare coarse problem
tcoarse= tic;  
Qmu= sparse(ngf+nge+ngv,Ns*ndtn); % ndtn basis for each subdomain
if ndtn~=0
    % coarse basis from local NtD
    for s= 1:Ns
        nsb= length(sfnodes{s})+length(senodes{s})+length(svnodes{s});
        opts.isreal=false;
%         if 1==s
%             dtnv= 1./eigs(@(x)MblocalNtD(x,s),nsb,nsb,'lm',opts);
%             figure, plot(dtnv,'ro');
%         end
        % eigenvectors of localNtD
        [Vc,Dc]= eigs(@(x)invMblocalDtN(x,s),nsb,ndtn,'sr',opts);
        disp('Dc='); disp(diag(Dc));
%         if 1==s
%             udtn= QN{s} * (UN{s}\(LN{s}\( PN{s}*(Lb{s}'*Vc(:,2) ))));
%             figure;
%             hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz;
%             [X,Y,Z]= meshgrid(xl:hx:xl+(xr-xl)/Nx+hx,yl:hy:yl+(yr-yl)/Ny,zl:hz:zl+(zr-zl)/Nz);
%             udtn= reshape(udtn,ny/Ny+1,nx/Nx+2,nz/Nz+1);
%             slice(X,Y,Z,real(udtn), 0.5, 0.5, 0.5);
%             xlabel('x'); ylabel('y'); zlabel('z');
%             colorbar;
%         end
        % Vc= localNtD(Vc,s); % to have local Dirichlet vectors
        Qmu(:,(s-1)*ndtn+(1:ndtn))= Rb{s}'*Vc; % to global interface
    end
    % assemble coarse problem and lu
    Fcc= Qmu'*Fsub(Qmu);
    [Lcc,Ucc,Pcc,Qcc]= lu(Fcc);
    clear Fcc;
end
toc(tcoarse);

%% initsolve

fprintf('\ninit solve');
tinit= tic;
rhsl= rhsl + T(sparse(ngf+nge+ngv,1),b);
rhsl0= rhsl;
toc(tinit);
fprintf('\ninit coarse solve');
tinit= tic;
lambda= sparse(size(rhsl,1),1); % initial guess
if ndtn~=0
    if isempty(cform) || 1==cform
        % form P'Flambda = P'd = d - F lambda0, with P':= I-F*Q*(Fc\(Q'*)
        rhsmu= Qmu'*rhsl0;
        mu= Qcc * (Ucc\(Lcc\( Pcc*rhsmu )));
        clear rhsmu;
        rhsl= Qmu*mu;
        rhsl= T(rhsl) - rhsl + rhsl0;
    else
        % form P*F*P*lambda = P (d-F*lambda0), with P:= I - Q*(Fc\(Q'*F*))
        lambda0= Qmu*(Qcc*(Ucc\(Lcc\( Pcc*(Qmu'*rhsl0) ))));
        rhsl= rhsl0 + T(lambda0) - lambda0;
        rhsl= rhsl - Qmu*(Qcc*(Ucc\(Lcc\( Pcc*(Qmu'*Fsub(rhsl)) ))));
        lambda= lambda - Qmu*(Qcc*(Ucc\(Lcc\( Pcc*(Qmu'*Fsub(lambda)) ))));
    end
end
toc(tinit);


%% GMRES iteration
fprintf('\ngmres with schwarzsubs\n');
tg= tic;
tol= 1e-6;  restart= [];  maxit= 200; 
[lambda,flag,relres,iter,resvec] = gmres(@F,rhsl,restart,tol,maxit,[],[],lambda); 
toc(tg);
% fname= sprintf('o%dl%dd.mat',ol,numplw);
% save(fname,'flag','relres','iter','resvec');
disp('iter='); disp(iter);
disp('relres='); disp(relres);

%% spectral picutre
if nargout>=6
    opts.isreal= false;
    specF= eigs(@F,ngf+nge+ngv,ngf+nge+ngv,'lm',opts);
end
%% recover u
if ndtn~=0
    if isempty(cform) || 1==cform
        % form P'F
        mu= mu - Qcc * (Ucc\(Lcc\( Pcc*( Qmu'*Fsub(lambda) ) )));
        lambda= lambda + Qmu*mu;
        clear mu;
    else
        % form PFP
        lambda= lambda + lambda0;
        clear lambda0;
    end
end
u= zeros(nn,1);
for s= 1:Ns
    us= SolveSub(s,lambda,b);
    u= u + Dw{s}.*(Ro{s}.'*us);
end

%% subfunctions
    function y= F(x)  
        y= x - T(x);
        % coarse solve 
        if ndtn~=0
            if isempty(cform) || cform==1
                % form P'F, with F:= I - T, P':= I - F*Q*(Fcc\(Q'*)
                z= Qmu'*y;
                z= Qcc * (Ucc\(Lcc\( Pcc*z )));
                z= Qmu*z;
                y= y - z + T(z);
            else
                % form PFP, with P:= I - Q*(Fcc\(Q'*(F*))), the first P can be
                % omitted if initial lambda is in the range of P, as we have
                % done in the above codes
                z= Qmu'*(y-T(y));
                z= Qcc * (Ucc\(Lcc\( Pcc*z )));
                z= Qmu*z;
                y= y - z;
            end
        end
    end

    function y= Fsub(x)
        % without coarse solve
        y= x - T(x);
    end

    function usub= SolveSub(sub,x,f)
        % subsolve using x as b.c., i.e. rhs nonzero at the interface,
        % and f{sub} as load
        % prepare rhs
        if 2==nargin
            bsub= sparse(size(Ro{sub},1),size(x,2));
        elseif 3==nargin
            bsub= f{sub};
        else
            disp('SolveSub: nargin must be 2 or 3');
        end
        if nnz(x)~=0
            bsub= bsub + Aget{sub} * x;
        end
        % solve
        usub= Q{sub} * (U{sub}\(L{sub}\( P{sub}*bsub )));
    end

    function y= T(x,f)
       % x consists of lambda defined on faces, edges and vertices
       % f{s} is the load for the subdomain s
       % y is new lambda communicated from the subsolutions with x 
       y= sparse(size(x,1),size(x,2));
       for sub= 1:Ns
           % subsolve
           if nargin==1
               usub= SolveSub(sub,x);
           elseif nargin==2
               usub= SolveSub(sub,x,f);
           else
               disp('T: nargin must be 1 or 2');
           end
           % calculate y from subdomain solutions, give neighbors their 
           %  interface values
           if nnz(usub)==0
               continue;
           end
           usub= sparse(Ro{sub}.'*usub);
           y= y + Agive{sub} * usub;
       end
    end

    function y= MblocalNtD(x,sub)
       bsub= Lb{sub}'*x; 
       usub= QN{sub} * (UN{sub}\(LN{sub}\( PN{sub}*bsub )));
       y= Mb{sub}*(Lb{sub}*usub);
    end

    function y= localNtD(x,sub)
       % Given Neumann data x on the local interface of the subdomain s,
       % this function solves the subproblem and outputs Dirichlet trace
       bsub= Lb{sub}'*x; 
       usub= QN{sub} * (UN{sub}\(LN{sub}\( PN{sub}*bsub )));
       y= Lb{sub}*usub;
    end
 
    function y= localDtN(x,sub)
       % Given Dirichlet data x on the local interface of the subdomain s,
       % this function solves the subproblem and outputs Neumann trace
       bsub= Lb{sub}'*x; 
       usub= Q{sub} * (U{sub}\(L{sub}\( P{sub}*bsub )));
       y= Lb{sub}*(AN{sub}*usub);
    end

    function y= invMblocalDtN(x,sub)
       % Given Dirichlet data x on the local interface of the subdomain s,
       % this function solves the subproblem and outputs Neumann trace
       bsub= Lb{sub}'*x; 
       usub= Q{sub} * (U{sub}\(L{sub}\( P{sub}*bsub )));
       y= Lb{sub}*(AN{sub}*usub);
       y= QMb{sub} * (UMb{sub}\( LMb{sub}\( PMb{sub}*y )));
    end


end  % -- end of obddsubs.m