function Ze= stima(vertices,k2)
% assemble brick element for the Helmholtz equation
% -\lap u - k^2 u = f,   in \Omega
% the reference element is 8-node brick on [-1,1]^3
% the nodes is ordered in
% [(-1,-1,-1);(1,-1,-1); (1,1,-1); (-1,1,-1); 
%  (-1,-1, 1);(1,-1, 1); (1,1, 1); (-1,1, 1)].
% DPhi [1+xi;1+et;1+ze] + [x1;y1;z1] maps the canonical brick node to 'vertices'
global A11 A12 A13 A21 A22 A23 A31 A32 A33 M;
% global gp gw gp1 gw1;
% persistent phi;
DPhi= [vertices(2,:)-vertices(1,:);vertices(4,:)-vertices(1,:);vertices(5,:)-vertices(1,:)]'/2;
detDPhi= det(DPhi);
B= inv(DPhi'*DPhi);
Ze= B(1,1)*A11+B(1,2)*A12+B(1,3)*A13+B(2,1)*A21+B(2,2)*A22+B(2,3)*A23+B(3,1)*A31+B(3,2)*A32+B(3,3)*A33;
Ze= detDPhi*Ze;
if isa(k2,'float')
    Ze= Ze - k2*detDPhi*M;
elseif isa(k2,'inline') || isa(k2,'function_handle')
    % use value of k2 at the center of the element
    Ze= Ze - k2(mean(vertices,1))*detDPhi*M;
%     % values of the eight basis functions at gauss quadrature points
%     if isempty(phi)
%         if ~isempty(gp1)
%             phi= fembasis(gp1);
%         else
%             phi= fembasis(gp);
%         end
%     end
%     % gauss quadrature points on the element of 'vertices'
%     if ~isempty(gp1)
%         DPhigp= (1+gp1)*DPhi'+repmat(vertices(1,:),size(gp1,1),1);
%         Ze= Ze - detDPhi*phi'*(repmat(gw1.*k2(DPhigp),1,8).*phi); 
%     else
%         DPhigp= (1+gp)*DPhi'+repmat(vertices(1,:),size(gp,1),1);
%         % DPhigp method 2: slower
% %         DPhigp= DPhi*(1+gp'); v1= vertices(1,:)';
% %         for m= 1:size(gp,1)
% %             DPhigp(:,m)= DPhigp(:,m) +  v1;
% %         end
% %         DPhigp= DPhigp.';
%         % faster than 'repmat' 
%         vk2= gw.*k2(DPhigp);
%         Ze= Ze - detDPhi*phi'*([vk2 vk2 vk2 vk2 vk2 vk2 vk2 vk2].*phi); 
%         % Ze method 2: slower
% %           vk2= diag(gw.* k2(DPhigp));
% %           Ze= Ze - detDPhi*phi'*vk2*phi;
%         % Ze method 3: slowest
% %           vk2= gw.* k2(DPhigp);
% %           Ze(:,1)= Ze(:,1) - detDPhi*phi'*(vk2.*phi(:,1)); 
% %           Ze(:,2)= Ze(:,2) - detDPhi*phi'*(vk2.*phi(:,2)); 
% %           Ze(:,3)= Ze(:,3) - detDPhi*phi'*(vk2.*phi(:,3));
% %           Ze(:,4)= Ze(:,4) - detDPhi*phi'*(vk2.*phi(:,4));
% %           Ze(:,5)= Ze(:,5) - detDPhi*phi'*(vk2.*phi(:,5));
% %           Ze(:,6)= Ze(:,6) - detDPhi*phi'*(vk2.*phi(:,6));
% %           Ze(:,7)= Ze(:,7) - detDPhi*phi'*(vk2.*phi(:,7));
% %           Ze(:,8)= Ze(:,8) - detDPhi*phi'*(vk2.*phi(:,8));
%     end
end
end