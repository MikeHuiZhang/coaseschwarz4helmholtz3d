function [sp1,sp2]= specDtN(w,h,H,L)
% spectral picutre for the DtN operator
% H is the subdomain size along dimension to be Fourier transformed
% L is the size of the subdomain along dimension decomposed from the domain
% L includes overlap

basefreq= pi/H;
kmax= sqrt(2)*pi/h;

% discrete freq.
n= floor(kmax/basefreq);
[m,n]= ndgrid(1:n,1:n);
k= sqrt(m.^2+n.^2)*basefreq;
k= k(:); k= k(k<=kmax);
kl= k(k<w); kh= k(k>=w);

% spectral picture
r1= -1i*sqrt(w^2-kl.^2); % k<w
r2= sqrt(kh.^2-w^2); % k>w
sp1= dtn(r1); 
sp2= dtn(r2);
figure, hold on, plot(sp1,'o');
plot(sp2,'ro');

    function re= dtn(r)  % formula by cont. Fourier, see Maple codes
        re= -r.*((-r+1i*w).*exp(r*L)+(r+1i*w).*exp(-r*L))./ ...
            ((r-1i*w).*exp(r*L)+(r+1i*w).*exp(-r*L));
    end

end