function re= ksquare(coord)
%This function reads(when called for the first time) the piecewise constant
% velocity from the input 'datafile' and gives the wavenumber= omega/velocity
% at the point specified by the input 'coord'
global data_array;                % change this file along with arobin.m
nz= 2; ny= 6; nx= 6;   freq= 0.25; % change here
if isempty(data_array) 
%     datafile= sprintf('data/seg%d_%d_%d',nz,ny,nx);
    datafile= sprintf('data/man%d_%d_%d_r1e8',nz,ny,nx);
    fid= fopen(datafile);  
    data_array= fread(fid,inf,'float64');
    fclose(fid);
end
hz= 4200/nz; hy= 13520/ny; hx= 13520/nx; 
iz= floor(coord(:,3)/hz)+1;
iy= floor(coord(:,2)/hy)+1;
ix= floor(coord(:,1)/hx)+1;
iz(iz>nz)= nz; 
iy(iy>ny)= ny; 
ix(ix>nx)= nx; 
ii= (ix-1)*ny*nz + (iy-1)*nz + iz; 
re= 2*pi*freq./data_array(ii); 
re= re.^2;
end    