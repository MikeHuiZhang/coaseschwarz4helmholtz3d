function re= rhs2d(vertices,uR)
% INPUT vertices:  3d coordinates of a rectangle element
%             uR:  Robin data on the element
% OUTPUT      re:  4 by 1 vector corresponding to (uR,v)
global gp2d gw2d;
% Jacobian of the transformation from [-1,1]^2 to the element in 3d
% coordinates, 3 by 2 matrix, repeated for stima2d.m
DPhi= [vertices(2,:)-vertices(1,:);vertices(4,:)-vertices(1,:)]'/2;
detDPhi= norm(DPhi(:,1))*norm(DPhi(:,2)); % ratio of areas 
if isa(uR,'float')
    re= detDPhi*uR*ones(4,1);
elseif isa(uR,'inline') || isa(uR,'function_handle')
    % values of the eight basis functions at gauss quadrature points
    phi= fembasis2d(gp2d);
    % gauss quadrature points on the element of 'vertices'
    DPhigp= (1+gp2d)*DPhi'+vertices(ones(size(gp2d,1),1),:);
    re= detDPhi*phi'*(gw2d.*uR(DPhigp));
end
end
